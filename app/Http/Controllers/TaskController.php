<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use Carbon\Carbon;

class TaskController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tasks = Task::with('user')->get();

        return view('task/index', compact('tasks'));
    }

    public function new()
    {
        $users = User::all();

        return view('task/create', compact('users'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'assign_user_id' => 'required',
            'task_title' => 'required|min:5',
            'task_description' => 'required|min:5',
        ]);

        Task::create([
            'user_id' => $request->get('assign_user_id'),
            'title' => $request->get('task_title'),
            'description' => $request->get('task_description'),
            'status' => 0,
        ]);

        return redirect()->route('task');
    }

    public function mytask()
    {
        $tasks = Task::where('user_id', auth()->id())->get();

        return view('task/own', compact('tasks'));
    }

    public function view(Request $request)
    {
        $id = $request->get('id');

        $task = Task::find($id)->first();

        return view('task/view', compact('task'));
    }

    public function setstatus(Request $request)
    {
        $id = $request->get('id');

        $task = Task::find($id);

        if ($task->status === 0)
        {
            $task->status = 1;
        }
        elseif ($task->status === 1) {
            $task->status = 2;
        }

        $task->save();

        return redirect()->route('mytask');

    }
}
