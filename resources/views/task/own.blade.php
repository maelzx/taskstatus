@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Task List</div>

                    <div class="card-body">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($tasks as $task)
                                <tr>
                                    <td>{{ $task->title }}</td>
                                    <td>
                                        @if ($task->status === 2)
                                            Complete
                                        @elseif ($task->status === 1)
                                            In Progress
                                        @else
                                            New
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-primary" onclick="event.preventDefault();document.getElementById('view-task-form-{{ $task->id }}').submit();">View</button>
                                        <button class="btn btn-info" onclick="event.preventDefault();document.getElementById('set-status-form-{{ $task->id }}').submit();">
                                            @if ($task->status === 2)
                                                Completed
                                            @elseif ($task->status === 1)
                                                Set as Complete
                                            @else
                                                Set as In Progress
                                            @endif
                                        </button>

                                        <form id="view-task-form-{{ $task->id }}" action="{{ route('viewtask') }}" method="POST" style="display: none;">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $task->id }}">
                                        </form>
                                        <form id="set-status-form-{{ $task->id }}" action="{{ route('settaskstatus') }}" method="POST" style="display: none;">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $task->id }}">
                                        </form>
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td rowspan="3">No task available</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
