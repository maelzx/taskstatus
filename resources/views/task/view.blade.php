@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">View Task</div>

                    <div class="card-body">

                        <p>Title: {{ $task->title }}</p>
                        <p>Status: {{ $task->status }}</p>
                        <p>Description: {{ $task->description }}</p>
                        <p>Created at: {{ Carbon\Carbon::parse($task->created_at)->format('d/m/Y h:i:s A') }}</p>
                        <p>Last Updated at: {{ Carbon\Carbon::parse($task->updated_at)->format('d/m/Y h:i:s A') }}</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
