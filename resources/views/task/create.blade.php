@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">New Task</div>

                    <div class="card-body">

                        <form method="post">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="assign_user_id">Assign To</label>
                                <select class="form-control" id="assign_user_id" name="assign_user_id">
                                    <option>Please Select User</option>
                                    @foreach ($users as $user)
                                        <option value="{{ $user->id }}">{{ $user->name  }} (#{{ $user->id }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="task_title">Task Title</label>
                                <input type="text" class="form-control" id="task_title" name="task_title">
                            </div>
                            <div class="form-group">
                                <label for="task_description">Task Description</label>
                                <input type="text" class="form-control" id="task_description" name="task_description">
                            </div>
                            <button type="submit" class="btn btn-primary">Create</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
