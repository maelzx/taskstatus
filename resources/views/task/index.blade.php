@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-default">
                    <div class="card-header">Task List</div>

                    <div class="card-body">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Assign To</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            @forelse ($tasks as $task)
                                <tr>
                                    <td>{{ $task->user->name }}</td>
                                    <td>{{ $task->title }}</td>
                                    <td>
                                        @if ($task->status === 2)
                                            Complete
                                        @elseif ($task->status === 1)
                                            In Progress
                                        @else
                                            New
                                        @endif
                                    </td>
                                </tr>
                            @empty
                                <tr>
                                    <td rowspan="3">No task available</td>
                                </tr>
                            @endforelse
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
