<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/task', 'TaskController@index')->name('task');
Route::get('/task/new', 'TaskController@new')->name('newtask');
Route::post('/task/new', 'TaskController@create');
Route::get('/task/own', 'TaskController@mytask')->name('mytask');
Route::post('/task/view', 'TaskController@view')->name('viewtask');
Route::post('/task/setstatus', 'TaskController@setstatus')->name('settaskstatus');
